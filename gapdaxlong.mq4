//+------------------------------------------------------------------+
//|                                                        Gap DAX 30|
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Sergio Iserte"
//#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#define PERIOD 15

bool signal1=false,signal2=false,updated=false;
double marketOpenPriceOpenBar,marketClosePriceCloseBar,auxPrice;
int ticket=0;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void performOrder(double price)
  {
   double minstoplevel=MarketInfo(Symbol(),MODE_STOPLEVEL);
   double stoploss=NormalizeDouble(Bid-minstoplevel*Point,Digits)-20;
   double takeprofit=NormalizeDouble(Bid+minstoplevel*Point,Digits)+40;
   ticket=OrderSend(Symbol(),OP_BUY,1,price,1,stoploss,takeprofit,"My order",16384,0,clrGreen);
  }
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

int OnInit()
  {

   Alert("Initiating GAP DAX long");
   Alert("WARNING - Be sure that period is M",PERIOD," - WARNING");
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

void OnDeinit(const int reason)
  {
   Alert("Function deinit() triggered at deinitialization");   // Alert
   return;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

void OnTick()
  {
   int HH=TimeHour(Time[0]);
   int mm=TimeMinute(Time[0]);

//Alert("Hora: ", HH," - Min: ", mm, " - - - - Hora de antes:", TimeHour(Time[1]), ":", TimeMinute(Time[1]));
//Alert("Time: ",Time[0]," - Open: ",Open[0]," - Close:",Close[0]," - - - ",updated);
//Alert("TICK: ",Ask," - ",Bid);

   if(ticket)
     {
      if(OrderSelect(ticket,SELECT_BY_TICKET))
        {
         if(OrderCloseTime()==0)
           {  //it is still open
            if((HH==20) && (mm==00))
              {  //time to close
               //Alert("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@",ticket);
               int errno=OrderClose(ticket,1,Bid,1,Red);
               ticket=0;
              }
           }
         else //ticket has been closed
         ticket=0;
        }
      return;
     }
/*
   if(HH>18)
     {
      signal1=false;
      signal2=false;
      return;
        } else if(HH==17){
      if(mm>31)
        {
         signal1=false;
         signal2=false;
         return;
        }
     }
*/
   if((HH==17) && (mm==(30+PERIOD)))
     { //new closing
      if(!updated) //with this we prevent a continuos update of the variable during the period
        {
         signal1=false; //reset signals because the day has ended
         signal2=false;
         marketClosePriceCloseBar=Close[1]; //we take the closing price of the previous flag
                                            //Alert("Close market time ",Time[1]," price ",marketClosePriceCloseBar);
         updated=true;
        }
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
/*   else if((HH==8) && (mm==00))
     { //new opening
      if(!updated)
        {
         marketOpenPriceOpenBar=Open[0];
         //Alert("Open market time ",Time[0]," price ",marketOpenPriceOpenBar);
         if(marketClosePriceCloseBar-marketOpenPriceOpenBar>=30)
           {
            signal1=true;
            //Alert("Signal 1: ENABLED");
           }
         updated=true;
        }
     }
*/
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else if((HH==8) && (mm==PERIOD))
     { //auxiliary price
      if(!updated)
        {
         marketOpenPriceOpenBar=Open[1];
         //Alert("Open market time ",Time[1]," price ",marketOpenPriceOpenBar);
         if((marketClosePriceCloseBar-30)>=marketOpenPriceOpenBar)
           {
            signal1=true;
            //Alert("Signal 1: ENABLED");

            auxPrice=High[1];  //maximum price in the candle of 8:15 and 8:30
                               //Alert("Signal market time ",Time[1]," price ",auxPrice);
            //if(auxPrice<=marketOpenPriceOpenBar)
              //{
               signal2=true;
               //double price=Ask;
               //if(price==(auxPrice-20))
               //  {
               //   performOrder(price);
               // Alert("Signal 2: ENABLED");
               //Alert("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
/*
                           double minstoplevel=MarketInfo(Symbol(),MODE_STOPLEVEL);
                           //double price=NormalizeDouble(Bid-minstoplevel*Point,Digits)-20;
                           double price=Ask;
                           double stoploss=NormalizeDouble(Bid-minstoplevel*Point,Digits)-20;
                           double takeprofit=NormalizeDouble(Bid+minstoplevel*Point,Digits)+40;
                           ticket=OrderSend(Symbol(),OP_BUY,1,price,3,stoploss,takeprofit,"My order",16384,0,clrGreen);
                           //+------------------------------------------------------------------+
                           if(ticket<0) Print("OrderSend failed with error #",GetLastError());
                           //else Print("OrderSend placed successfully");
                           //+------------------------------------------------------------------+
                           */
               // }
              //}
           }
         updated=true;
        }
     }
   else
     {
      updated=false;

      if(signal2)
        {
         double price=Ask;
         if(price==(auxPrice-20))
           {
            performOrder(price);
/*
            double minstoplevel=MarketInfo(Symbol(),MODE_STOPLEVEL);
            double stoploss=NormalizeDouble(Bid-minstoplevel*Point,Digits)-20;
            double takeprofit=NormalizeDouble(Bid+minstoplevel*Point,Digits)+40;
            ticket=OrderSend(Symbol(),OP_BUY,1,price,3,stoploss,takeprofit,"My order",16384,0,clrGreen);
            */
            signal1=false;
            signal2=false;
           }
        }
     }
   return;
  }
//+------------------------------------------------------------------+
